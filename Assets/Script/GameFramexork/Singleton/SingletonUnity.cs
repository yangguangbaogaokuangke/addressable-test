﻿using UnityEngine;
using System.Collections;

public class SingletonUnity<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T instance = null;
	
	public static T Instance
    {	
		get
        {
            if (instance == null)
            {
				instance = GameObject.FindObjectOfType<T>();    
                
                if (instance == null)
                {
                    GameObject go = new GameObject("Temp");
                    instance = go.AddComponent<T>();
                    go.name = instance.GetType().FullName;
                }
			}
			
			return instance;
		}		
	}

    public static bool HaveInstance()
    {
        return instance != null;
    }

    public static void ClearSingleton()
    {
        instance = null;
	}
}
