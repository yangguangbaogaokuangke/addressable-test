﻿using UnityEngine;
using System.Collections;
using System;

public class Singleton<T> where T : new()
{
    private static T instance = default(T);

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new T();
            }

            return instance;
        }
    }

    public static bool HaveInstance()
    {
        return instance != null;
    }

    public static void ClearSingleton()
    {
        instance = default(T);
    }
}
