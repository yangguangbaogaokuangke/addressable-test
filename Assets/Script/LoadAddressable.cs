using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;

public class LoadAddressable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.StartCoroutine(Time());
        this.StartCoroutine(loadTexture());
        this.StartCoroutine(LoadPrefab());

        //var obj = ResourceManager.Instance.LoadObject("testCube");
        //GameObject.Instantiate(obj);
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnDestroy()
    {
        ResourceManager.Instance.Close();
    }

    private void Load() 
    {
        //ResourceManager.Instance.LoadSpriteAsync("mainMenu", "anniu_worldmap", (sprite) => 
        //{
        //    gameObject.GetComponent<Image>().sprite = sprite;
        //});
        gameObject.GetComponent<Image>().sprite = ResourceManager.Instance.LoadSprite("mainMenu", "anniu_worldmap");

        var cube = (GameObject)Instantiate(ResourceManager.Instance.LoadObj("testCube2"));
        cube.transform.position = new Vector3(-240, 100, 500);
        cube.transform.localScale = new Vector3(100, 100, 100);
    }

    private void LoadedTexture(AsyncOperationHandle<SpriteAtlas> obj)
    {
        gameObject.GetComponent<Image>().sprite = obj.Result.GetSprite("anniu_worldmap");
    }
    private IEnumerator loadTexture()
    {
        yield return new WaitForSeconds(2.0f);
        Load();
        yield return new WaitForSeconds(2.0f);
        gameObject.GetComponent<Image>().sprite = ResourceManager.Instance.LoadSprite("mainMenu", "anniu_gonghui");

        yield return new WaitForSeconds(2.0f);
        gameObject.GetComponent<Image>().sprite = ResourceManager.Instance.LoadSpriteAtlas("mainMenu").GetSprite("anniu_gonghuizhan");

        yield return new WaitForSeconds(2.0f);
        gameObject.GetComponent<Image>().sprite = ResourceManager.Instance.LoadSprite("mainMenu", "anniu_guaiwu");

        yield return new WaitForSeconds(2.0f);
        gameObject.GetComponent<Image>().sprite = ResourceManager.Instance.LoadSpriteAtlas("mainMenu").GetSprite("anniu_haoyou");

        yield return new WaitForSeconds(2.0f);
        ResourceManager.Instance.LoadSpriteAsync("mainMenu", "anniu_libao", (sprite)=> 
        {
            gameObject.GetComponent<Image>().sprite = sprite;
        });

        yield return new WaitForSeconds(2.0f);
        ResourceManager.Instance.LoadSpriteAsync("mainMenu", "111", (sprite) =>
        {
            gameObject.GetComponent<Image>().sprite = sprite;
        });
    }
    float time = 0;
    private IEnumerator Time()
    {
        while (true)
        {
            Debug.Log((time += 1.0f).ToString());
            yield return new WaitForSeconds(1.0f);
        }
    }
    private IEnumerator LoadPrefab()
    {
        yield return new WaitForSeconds(3.0f);
        //ResourceManager.Instance.LoadObjectAsync("testCube", (obj) => {
        //    var cube = (GameObject)Instantiate(obj);
        //    cube.transform.position = new Vector3(-240, 100, 500);
        //    cube.transform.localScale = new Vector3(100, 100, 100);
        //}); 
    }
}
