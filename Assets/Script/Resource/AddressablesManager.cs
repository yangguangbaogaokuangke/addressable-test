﻿using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Collections.Generic;

class AddressablesManager:Singleton<AddressablesManager>
{
    public AddressablesManager()
    {
    }
    /// <summary>
    /// 同步加载模型
    /// </summary>
    /// <param name="resPath"></param>
    /// <returns></returns>
    public UnityEngine.Object LoadObject(string resPath)
    {
        var op = Addressables.LoadAssetAsync<Object>(resPath);
        op.WaitForCompletion();

        if (op.Result == null)
        {
            Debug.LogError("ResourceManager:LoadObject:op.Result == null   resPath = " + resPath);
            return null;
        }

        var obj = op.Result;
        Addressables.Release(op);
        return obj;
    }
    /// <summary>
    /// 异步加载模型
    /// </summary>
    /// <param name="resPath"></param>
    /// <param name="callBack"></param>
    public void LoadObjectAsync(string resPath, System.Action<Object> callBack = null)
    {
        var op = Addressables.LoadAssetAsync<Object>(resPath);
        op.Completed += (obj) => {
            if (op.Result == null)
                Debug.LogError("ResourceManager:LoadAssetAsync:op.Result == null   resPath = " + resPath);
            else
            {
                if (callBack == null)
                    Debug.LogError("ResourceManager:LoadObjectAsync:callBack == null");
                else
                {
                    callBack(op.Result);
                    Addressables.Release(op);
                }
            }
        };
    }

    public void Close()
    {
    }
}
