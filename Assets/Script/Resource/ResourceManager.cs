using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;

public class ResourceManager
{
    private static ResourceManager instance { get; set; }
    public static ResourceManager Instance
    {
        get 
        {
            if (instance == null)
                instance = new ResourceManager();
            return instance;
        }
    }
    private ResourceManager()
    {
    }
    public void Close()
    {
        var keys = new List<string>();
        keys.AddRange(this.objDic.Keys);

        for (int i = 0; i < keys.Count; ++i)
            objDic[keys[i]].Dispos();
        objDic.Clear();
    }

    private Dictionary<string, Resource> objDic = new Dictionary<string, Resource>();
    public GameObject LoadObj(string objName)
    {
        if (objDic.ContainsKey(objName))
        {
            var res = (NormalResource)objDic[objName];
            if (res.isLoad)
                return res.gameObject;
            else
                objDic.Remove(objName);
        }

        var resource = new NormalResource(objName);
        resource.Load();
        if (resource.isLoad)
            objDic.Add(objName, resource);
        else
        {
            Debug.LogError("ResourceManager:LoadObj:isLoad == false:����ģ��ʧ�ܣ�����Ѱַ·������--->" + objName);
            return null;
        }
        return resource.gameObject;
    }
    public void LoadObjectAsync(string objName, System.Action<Object> callBack = null)
    {
        if (objDic.ContainsKey(objName))
        {
            var res = (NormalResource)objDic[objName];
            if (res.isLoad)
            {
                callBack(res.gameObject);
                return;
            }
            else
                objDic.Remove(objName);
        }

        var resource = new NormalResource(objName);
        resource.LoadAsync((obj) =>
        {
            if (resource.isLoad)
                objDic.Add(objName, resource);
            else
                Debug.LogError("ResourceManager:LoadObjectAsync:isLoad == false:�첽����ģ��ʧ�ܣ�����Ѱַ·������--->" + objName);
            callBack(resource.gameObject);
        });
    }
    public SpriteAtlas LoadSpriteAtlas(string atlasName)
    {
        if (objDic.ContainsKey(atlasName))
        {
            var atlas = ((AtlasResoure)objDic[atlasName]);
            if (atlas.isLoad)
                return atlas.atlas;
            else
                objDic.Remove(atlasName);
        }

        var resource = new AtlasResoure(atlasName);
        resource.Load();

        if (resource.isLoad)
            objDic.Add(atlasName, resource);
        else
        {
            Debug.LogError("ResourceManager:LoadSpriteAtlas:isLoad == false:����ͼ��ʧ�ܣ�����Ѱַ·������--->" + atlasName);
            return null;
        }
        return resource.atlas;
    }
    public void LoadSpriteAtlasAsync(string atlasName, UnityEngine.Events.UnityAction<SpriteAtlas> action)
    {
        if (objDic.ContainsKey(atlasName))
        {
            var res = (AtlasResoure)objDic[atlasName];
            if (res.isLoad)
            {
                action(res.atlas);
                return;
            }
            else
                objDic.Remove(atlasName);
        }
        var resource = new AtlasResoure(atlasName);
        resource.LoadAsync((obj) =>
        {
            if (resource.isLoad)
                objDic.Add(atlasName, resource);
            else
                Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼ��ʧ�ܣ�����Ѱַ·������--->" + atlasName);
            action(resource.atlas);
        });
    }
    public Sprite LoadSprite(string atlasName, string spriteName)
    {
        if (objDic.ContainsKey(atlasName))
        {
            var res = (AtlasResoure)objDic[atlasName];
            if (res.isLoad)
            {
                var sprite = res.atlas.GetSprite(spriteName);
                if (sprite == null)
                    Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼƬʧ�ܣ�����Ѱַ·������--->atlasName = " + atlasName + "   ͼƬ�� = " + spriteName);
                return sprite;
            }
        }

        var resource = new AtlasResoure(atlasName);
        resource.Load();
        if (resource.isLoad)
        {
            objDic.Add(atlasName, resource);
            var sprite = resource.atlas.GetSprite(spriteName);
            if (sprite == null)
                Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼƬʧ�ܣ�����Ѱַ·������--->atlasName = " + atlasName + "   ͼƬ�� = " + spriteName);
            return sprite;
        }
        Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼ��ʧ�ܣ�����Ѱַ·������--->" + atlasName);
        return null;
    }
    public void LoadSpriteAsync(string atlasName, string spriteName, UnityEngine.Events.UnityAction<Sprite> action)
    {
        if (objDic.ContainsKey(atlasName))
        {
            var res = (AtlasResoure)objDic[atlasName];
            if (res.isLoad)
            {
                var sprite = res.atlas.GetSprite(spriteName);
                if (sprite == null)
                    Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼƬʧ�ܣ�����Ѱַ·������--->atlasName = " + atlasName + "   ͼƬ�� = " + spriteName);
                action(sprite);
                return;
            }
            else
                objDic.Remove(atlasName);
        }

        var resource = new AtlasResoure(atlasName);
        resource.LoadAsync((obj) =>
        {
            if (resource.isLoad)
            { 
                objDic.Add(atlasName, resource);
                var sprite = resource.atlas.GetSprite(spriteName);
                if (sprite == null)
                    Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼƬʧ�ܣ�����Ѱַ·������--->atlasName = " + atlasName + "   ͼƬ�� = " + spriteName);
                action(sprite);
            }
            Debug.LogError("ResourceManager:LoadSpriteAtlasAsync:isLoad == false:����ͼ��ʧ�ܣ�����Ѱַ·������--->" + atlasName);
            action(null);
        });

    }
    public void DisposObj(string objName)
    {
        if (objDic.ContainsKey(objName))
        {
            var resource = objDic[objName];
            resource.num = 0;
            resource.Dispos();
            objDic.Remove(objName);
        }
    }
}
