﻿using UnityEngine;
using UnityEngine.Events;

public abstract class Resource
{
    public string name { get; set; }
    //引用计数
    public int num { get; set; }
    protected UnityEngine.Object MainObj { get; set; }
    public bool isLoad
    {
        get {
            return MainObj != null;
        }
    }
    public virtual UnityEngine.Object Load()
    {
        return MainObj;
    }
    public virtual void LoadAsync(UnityEngine.Events.UnityAction<Object> action)
    {
        action(MainObj);
    }
    public virtual void Dispos()
    {
        if (MainObj != null)
        {
            if (num == 0)
                UnityEngine.GameObject.Destroy(MainObj);
            else
                num -= 1;
        }
    }
}
public class AtlasResoure : Resource
{
    public AtlasResoure(string atlasName)
    {
        name = atlasName;
    }
    public UnityEngine.U2D.SpriteAtlas atlas 
    {
        get {

            if (MainObj == null)
                return null;
            else
                num += 1;
            return (UnityEngine.U2D.SpriteAtlas)MainObj;
        }
    }
    private void LoadSprite()
    {
        MainObj = AddressablesManager.Instance.LoadObject(name);
        if (MainObj != null)
            num = 0;
    }
    private void LoadSpriteAsync(UnityAction<Object> action)
    {
        AddressablesManager.Instance.LoadObjectAsync(name, (obj) =>
        {
            action(obj);
        });
    }

    public override UnityEngine.Object Load()
    {
        if (MainObj == null)
            LoadSprite();
        return MainObj;
    }
    public override void LoadAsync(UnityAction<Object> action)
    {
        if (MainObj == null)
            LoadAsync((obj) => 
            {
                MainObj = obj;
                if (MainObj != null)
                    num = 0;
            });
        action(MainObj);
    }
}

public class NormalResource : Resource
{
    public NormalResource(string objName)
    {
        name = objName;
    }
    public UnityEngine.GameObject gameObject
    {
        get
        {
            if (MainObj == null)
                return null;
            else
                num += 1;
            return (UnityEngine.GameObject)MainObj;
        }
    }
    private void LoadGameObj()
    {
        MainObj = AddressablesManager.Instance.LoadObject(name);
        if (MainObj != null)
            num = 0;
    }
    private void LoadGameObj(UnityAction<Object> action)
    {
        AddressablesManager.Instance.LoadObjectAsync(name, (Object) =>
        {
            action(Object);
        });
    }
    public override Object Load()
    {
        if (MainObj == null)
            LoadGameObj();
        return MainObj;
    }
    public override void LoadAsync(UnityAction<Object> action)
    {
        if (MainObj == null)
            LoadAsync((Object) => 
            {
                MainObj = Object;
                if (MainObj != null)
                    num = 0;
            });
    }
}